#include <stdio.h>

/*
 * 9524:\>_
 * Computer Science
 * Fudan University
 */
int main(int argc, char *argv[])
{
    int yrs = 0x07e3 - 0x07cf; 
    printf("%dth Anniversary\n", yrs);
    return 0;
}
